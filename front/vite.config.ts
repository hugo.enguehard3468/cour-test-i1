import react from "@vitejs/plugin-react";
import visualizer from "rollup-plugin-visualizer";
import { defineConfig } from "vite";

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [react()],
    server: {
        // https: true,
        fs: {
            // Activer le serveur de fichiers statiques
            strict: true
        }
    },
    build: {
        chunkSizeWarningLimit: 1000, // Limite en kilo-octets
        rollupOptions: {
            plugins: [
                visualizer({
                    open: true, // Cela ouvre automatiquement le rapport dans le navigateur après le build
                    gzipSize: true, // Affiche la taille des fichiers après compression gzip
                    brotliSize: true // Affiche la taille des fichiers après compression Brotli
                    // Vous pouvez ajouter d"autres options selon vos besoins
                })
            ],
            output: {
                manualChunks(id) {
                    if (id.includes("node_modules")) {
                        // Toutes les autres dépendances de node_modules
                        return "vendor";
                    }
                }
            }
        }
    }
});
