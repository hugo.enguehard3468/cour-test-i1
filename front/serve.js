"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var path = require("path");
var cors = require("cors");
var app = express();
var rootDir = path.resolve();
app.use(cors({ origin: "*" }));
app.use("/", express.static(path.join(rootDir, "dist")));
app.get("/*", function (_req, res) {
    res.sendFile(path.join(rootDir, "dist", "index.html"));
});
var port = process.env.PORT || 3000;
app.listen(port);
