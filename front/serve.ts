import * as express from "express";
import * as path from "path";
import * as cors from "cors";
const app = express();

const rootDir = path.resolve();

app.use(cors({ origin: "*" }));
app.use("/", express.static(path.join(rootDir, "dist")));

app.get("/*", (_req, res) => {
    res.sendFile(path.join(rootDir, "dist", "index.html"));
});

const port = process.env.PORT || 3000;

app.listen(port);
