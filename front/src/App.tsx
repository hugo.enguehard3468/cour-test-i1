import React, { Suspense, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import { ScreenEnum } from "./types/enums/screenEnum";

import { homePage, notFoundRoute } from "./utils/routes/routesConstants";
import { useSharedLogic } from "./utils/sharedLogic/sharedLogic";

import Layout from "./components/layout/layout";
import Home from "./pages/home/home";
import NotFound from "./pages/notFound/notFound";
import { RootState } from "./store/index";
import { setScreen } from "./store/slices/screenSlice";

const App: React.FC = () => {
    const language = useSelector((state: RootState) => state.language.value);
    // Call the custom hook to get the applyLanguage function
    const { applyLanguage } = useSharedLogic();

    const dispatch = useDispatch();
    useEffect(() => {
        function handleResize() {
            if (window.innerWidth < 768) {
                // pour téléphone (portrait)
                dispatch(setScreen(ScreenEnum.PHONE));
            } else if (window.innerWidth >= 768 && window.innerWidth <= 1024) {
                // pour tablette
                dispatch(setScreen(ScreenEnum.TABLETTE));
            } else {
                // pour PC
                dispatch(setScreen(ScreenEnum.PC));
            }
        }

        window.addEventListener("resize", handleResize);
        handleResize();
        applyLanguage();

        return () => {
            window.removeEventListener("resize", handleResize);
        };
    }, []);

    return (
        <BrowserRouter>
            {language !== null && (
                <Suspense>
                    <Layout>
                        <Routes>
                            <Route path={homePage} element={<Home />} />
                            <Route path={notFoundRoute} element={<NotFound />} />
                        </Routes>
                    </Layout>
                </Suspense>
            )}
        </BrowserRouter>
    );
};
export default App;
