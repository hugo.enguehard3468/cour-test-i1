import React from "react";
import ReactDOM from "react-dom/client";

import AppBase from "./AppBase";
import "./index.css";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(<AppBase />);
