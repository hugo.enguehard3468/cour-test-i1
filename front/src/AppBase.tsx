import React from "react";
import { I18nextProvider } from "react-i18next";
import { Provider } from "react-redux";
import "remixicon/fonts/remixicon.css";

import i18n from "./utils/translate/i8n";

import App from "./App";
import { store } from "./store";

import "./App.scss";

const AppBase: React.FC = () => {
    const renderApp = () => (
        <I18nextProvider i18n={i18n}>
            <Provider store={store}>
                <App />
            </Provider>
        </I18nextProvider>
    );
    return renderApp();
};

export default AppBase;
