import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";

import { Lang } from "../../types/enums/langEnum";

import enTranslations from "./en.json";
import frTranslations from "./fr.json";

function normalizeLanguageCode(langCode: string): Lang {
    if (langCode.includes("-")) {
        return langCode.split("-")[0] as Lang;
    }
    return langCode as Lang;
}

i18n.use(LanguageDetector) // Ajoute le détecteur de langue
    .use(initReactI18next)
    .init({
        fallbackLng: Lang.EN,
        debug: import.meta.env.VITE_MODE === "DEV" ? true : false,
        interpolation: {
            escapeValue: false
        }
    });

// Option pour normaliser le code de langue détecté
i18n.on("languageChanged", (lng) => {
    const normalizedLang = normalizeLanguageCode(lng);
    if (lng !== normalizedLang) {
        i18n.changeLanguage(normalizedLang);
    }
});

i18n.addResourceBundle(Lang.EN, "translation", enTranslations);
i18n.addResourceBundle(Lang.FR, "translation", frTranslations);

export default i18n;
