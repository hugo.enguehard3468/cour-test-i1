import { RootState } from "../../store";

function assertIsDefined<T>(val: T): asserts val is NonNullable<T> {
    if (val === undefined || val === null) {
        throw new Error(`Expected "val" to be defined, but received ${val}`);
    }
}
export const selectLanguage = (state: RootState) => {
    const language = state.language.value;
    assertIsDefined(language);
    return language;
};

export const selectScreen = (state: RootState) => {
    const screen = state.screen.value;
    assertIsDefined(screen);
    return screen;
};