export const convertStringToEnum = <T>(str: string, enumMap: Record<string, T>): T | null => {
    const enumValue = enumMap[str];
    if (enumValue) {
        return enumValue;
    }
    return null;
};

export const convertStringToEnumValue = <T>(
    input: string,
    enumeration: { [s: string]: T }
): T | undefined => {
    return Object.values(enumeration).find((value) => value === input);
};
