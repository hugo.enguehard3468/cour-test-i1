//need to be called when logout
export const clearStorage = () => {
    // array of preserved keys
    const preserveKeys = ["i18nextLng"];
    for (let i = 0; i < localStorage.length; i++) {
        const key = localStorage.key(i);
        if (key && !preserveKeys.includes(key)) {
            localStorage.removeItem(key);
            i--;
        }
    }
};
