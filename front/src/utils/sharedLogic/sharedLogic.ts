import { useDispatch } from "react-redux";

import { Lang } from "../../types/enums/langEnum";

import { convertStringToEnumValue } from "../../utils/functions/convertStringToEnum";
import { localStorageLanguage } from "../../utils/localstorage/localStorageValues";

import { setLanguage } from "../../store/slices/languageSlice";
import i18n from "../translate/i8n";

export const useSharedLogic = () => {
    const dispatch = useDispatch();

    const applyLanguage = () => {
        if (localStorage.getItem(localStorageLanguage) === null) {
            localStorage.setItem(localStorageLanguage, i18n.language);
            const langEnum = convertStringToEnumValue(i18n.language, Lang);

            if (langEnum) dispatch(setLanguage(langEnum));
        } else {
            const lang = localStorage.getItem(localStorageLanguage);
            if (lang) {
                i18n.changeLanguage(lang);
                dispatch(setLanguage(lang as Lang));
            }
        }
    };

    return {
        applyLanguage
    };
};
