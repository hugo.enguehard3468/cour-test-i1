import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";

import languageReducer from "./slices/languageSlice";
import screenReducer from "./slices/screenSlice";

const rootReducer = combineReducers({
    language: languageReducer,
    screen: screenReducer
});

export const store = configureStore({
    reducer: rootReducer
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
