import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Lang } from "../../types/enums/langEnum";

interface LanguageState {
    value: Lang | null;
}
const initialState: LanguageState = {
    value: null
};
export const languageSlice = createSlice({
    name: "language",
    initialState,
    reducers: {
        setLanguage: (state, action: PayloadAction<Lang | null>) => {
            state.value = action.payload;
        }
    }
});

export const { setLanguage } = languageSlice.actions;
export default languageSlice.reducer;
