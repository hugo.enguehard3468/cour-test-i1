import { PayloadAction, createSlice } from "@reduxjs/toolkit";

import { ScreenEnum } from "../../types/enums/screenEnum";

interface ScreenState {
    value: ScreenEnum;
}
const initialState: ScreenState = {
    value: ScreenEnum.PC
};
export const ScreenSlice = createSlice({
    name: "screen",
    initialState,
    reducers: {
        setScreen: (state, action: PayloadAction<ScreenEnum>) => {
            state.value = action.payload;
        }
    }
});

export const { setScreen } = ScreenSlice.actions;
export default ScreenSlice.reducer;
