import React from "react";
import { useNavigate } from "react-router-dom";

import "./notFound.scss";

const NotFound: React.FC = () => {
    const navigate = useNavigate();
    const returnToMainPage = () => {
        navigate("/");
    };

    return (
        <div className="not-found">
            <button type="submit" onClick={returnToMainPage}>
                Page invalide
            </button>
        </div>
    );
};

export default NotFound;
